// 使用了动态导包，保证了数据不会在导包的同时完成加载。
async function relationship(obj) {
    if (!this.m)
        this.m = await import('./src/main/ets/relationship.js');
    return this.m.relationship(obj);
}

// 用了动态导包
export { relationship as default }

